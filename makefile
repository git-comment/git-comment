all: build

bootstrap:
	@cmake -Bbuild -GNinja

.PHONY: build
build:
	@ninja -Cbuild

.PHONY: clean
clean:
	@ninja -Cbuild clean

.PHONY: test
test: build
	@./build/libgitcomment/gitcomment-tests $(TESTSCOPE)
