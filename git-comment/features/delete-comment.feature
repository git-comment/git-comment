Feature: Deleting an existing comment
    Comments should be removable by specifying `--delete` with a
    comment identifier

    Scenario: Deletion when message is specified
        Given I have a git repository
        When I run git comment with "--delete 3342 --message foo"
        Then I receive output "fatal: Aborting due to incompatible options. --delete cannot be specified with other options."
        And git comment exits with status 1

    Scenario: Deletion when not in a git repository

    Scenario: Deleting an identifier for an object other than a comment

    Scenario: Deleting a non-existent identifier
        Given I have a git repository
        When I run git comment with "--delete 3342"
        Then I receive output "fatal: Could not resolve object identifier as a commit."
        And git comment exits with status 1

    Scenario: Deleting a comment
        Given I have a git repository
        When I run git comment with "--message 'hello hello'"
        Then I receive output indicating a comment was created
        And git comment exits with status 0
