#include "option_parser.h"

#include <stdlib.h>

void set_message(command_t *cmd) {
  ((struct comment_opts *)cmd->data)->message = cmd->arg;
}

void set_author(command_t *cmd) {
  ((struct comment_opts *)cmd->data)->author_signature = cmd->arg;
}

void set_commit(command_t *cmd) {
  ((struct comment_opts *)cmd->data)->commit_ref = cmd->arg;
}

void set_amended(command_t *cmd) {
  ((struct comment_opts *)cmd->data)->amend_comment_id = cmd->arg;
}

void set_deleted(command_t *cmd) {
  ((struct comment_opts *)cmd->data)->delete_comment_id = cmd->arg;
}

void set_mark_deleted_line(command_t *cmd) {
  ((struct comment_opts *)cmd->data)->mark_deleted_line = true;
}

cmdline_parse_result load_cmdline_options(const char *version,
                                          struct comment_opts *opts, int argc,
                                          char **argv) {
  command_t cmd = {.data = opts};
  command_init(&cmd, "git comment", version);
  command_option(&cmd, "-m", "--message [msg]", "Comment text", set_message);
  command_option(&cmd, "-c", "--commit [commit]", "Commit to annotate",
                 set_commit);
  command_option(
      &cmd, NULL, "--mark-deleted-line",
      "Add comment to the deleted version of the file and line number",
      set_mark_deleted_line);
  command_option(&cmd, NULL, "--amend [comment]", "Edit an existing comment",
                 set_amended);
  command_option(&cmd, NULL, "--author [author]", "Override the comment author",
                 set_author);
  command_option(&cmd, "-d", "--delete [comment]", "Remove a comment",
                 set_deleted);
  cmd.usage =
      "\t[-m <msg>] [--amend <comment>] [-c <commit>]\n"
      "\t\t\t[--author=<author>] [<filepath:line> [--mark-deleted-line]]\n"
      "\t git comment\t--delete <comment>\n"
      "\t git comment\t--help\n"
      "\t git comment\t--version";

  command_parse(&cmd, argc, argv);
  if (cmd.argc > 1) {
    return cmdline_parse_result_new_err((comment_opts_error) {
        "Unknown arguments specified", cmd
    });
  } else if (cmd.argc > 0) {
    gc_option_fileref ref = git_comment_fileref_fromstr(cmd.argv[0]);
    if (gc_option_fileref_is_none(ref)) {
      return cmdline_parse_result_new_err((comment_opts_error) {
          "Unable to parse file reference into format 'file:line'", cmd
      });
    }
    opts->fileref = ref.value;
  }

  return cmdline_parse_result_new_ok(cmd);
}
