#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <commander/commander.h>
#include <git2.h>
#include <gitcomment/comment.h>
#include <option.h>
#include <result.h>

#include "editor.h"
#include "option_parser.h"
#include "repo.h"

#define VERSION "2.0.0"

const char *MISSING_USER_ERROR =
    "unable to find an identity to attribute the comment.\n"
    "Configure a name and email using git config:\n"
    "\n"
    "    git config --global user.name \"Your Name\"\n"
    "    git config --global user.email you@example.com\n";

/**
 * Print a message an exit with failure status
 */
void exit_with_message(const char *format, ...)
    __attribute__((format(printf, 1, 2)));

void exit_with_message(const char *format, ...) {
  char *full_format = calloc(strlen(format) + 9, sizeof(char));
  sprintf(full_format, "fatal: %s\n", format);
  va_list args;
  va_start(args, format);
  vprintf(full_format, args);
  va_end(args);
  free(full_format);
  exit(EXIT_FAILURE);
}

void save_comment(struct comment_opts *opts) {
  git_repo_result repo_result = find_git_repository();
  if (git_repo_result_is_err(repo_result)) {
    exit_with_message(
        "not a git repository (or any of the parent directories)");
  }
  git_repository *repo = repo_result.value;
  git_comment *comment = calloc(1, sizeof(git_comment));
  comment->fileref = opts->fileref;

  git_object_result commit = find_commit_by_ref(repo, opts->commit_ref);
  if (git_object_result_is_err(commit)) {
    git_comment_free(comment);
    if (opts->commit_ref && strlen(opts->commit_ref) > 0) {
      exit_with_message("unknown or ambiguous revision '%s'", opts->commit_ref);
    } else {
      exit_with_message("no commits yet");
    }
  }
  memcpy(&comment->commit, commit.value, sizeof(git_oid));
  git_object_free(commit.value);

  gc_option_signature author =
      find_author_signature(repo, opts->author_signature);
  if (gc_option_signature_is_none(author)) {
    git_comment_free(comment);
    if (opts->author_signature && strlen(opts->author_signature) > 0) {
      exit_with_message("unable to parse '%s' into the format "
                        "'A Name <me@example.com>'",
                        opts->author_signature);
    } else {
      exit_with_message("%s", MISSING_USER_ERROR);
    }
  }
  comment->author = author.value;

  gc_option_signature amender = find_amender_signature(repo, NULL);
  if (gc_option_signature_is_none(amender)) {
    git_comment_free(comment);
    exit_with_message("%s", MISSING_USER_ERROR);
  }
  comment->amender = amender.value;

  comment->content = (char *)opts->message;
  if (!comment->content) {
    editor_message result = get_message_from_editor(repo);
    if (editor_message_is_ok(result)) {
      comment->content = result.value;
    } else {
      git_comment_free(comment);
      exit_with_message("%s", result.error);
    }
  }
  if (strlen(comment->content) == 0) {
    git_comment_free(comment);
    exit_with_message("aborting due to empty comment");
  }
  printf("composing comment authored by %s <%s>\n", author.value->name,
         author.value->email);
  printf("                 committed by %s <%s>\n", amender.value->name,
         amender.value->email);
  printf("\n'%s'\n", comment->content);
  git_comment_free(comment);
  git_repository_free(repo_result.value);
}

void amend_comment(struct comment_opts *opts) {}

void delete_comment(struct comment_opts *opts) {
  if (opts->message != NULL || opts->amend_comment_id != NULL ||
      opts->author_signature != NULL || opts->fileref.line > 0) {
    exit_with_message("Aborting due to incompatible options. --delete cannot "
                      "be specified with other options or arguments.");
  }
}

int main(int argc, char *argv[]) {
  struct comment_opts opts = {};
  memset(&opts, 0, sizeof(struct comment_opts));

  cmdline_parse_result cmd = load_cmdline_options(VERSION, &opts, argc, argv);
  if (cmdline_parse_result_is_err(cmd)) {
    printf("fatal: %s\n", cmd.error.message);
    command_print_help(&cmd.error.cmd);
    command_free(&cmd.error.cmd);
    exit(1);
  }

  git_comment_init();
  if (opts.delete_comment_id) {
    delete_comment(&opts);
  } else if (opts.amend_comment_id) {
    amend_comment(&opts);
  } else {
    save_comment(&opts);
  }

  command_free(&cmd.value);

  return EXIT_SUCCESS;
}
