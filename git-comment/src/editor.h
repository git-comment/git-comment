#pragma once
#include <git2/types.h>
#include <result.h>

define_result_type(editor_message, char *, char *);

editor_message get_message_from_editor(git_repository *repo);
