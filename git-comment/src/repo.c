#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "repo.h"

git_object_result find_commit_by_ref(git_repository *repo, const char *ref) {
  git_object *obj = NULL;
  const char *expanded_ref = ref && strlen(ref) > 0 ? ref : "HEAD";
  int error = git_revparse_single(&obj, repo, expanded_ref);
  if (error == 0) {
    if (git_object_type(obj) == GIT_OBJECT_COMMIT) {
      return git_object_result_new_ok(obj);
    } else {
      return git_object_result_new_err(-1);
    }
  }
  return git_object_result_new_err(error);
}

git_repo_result find_git_repository() {
  char *workdir = getcwd(NULL, 0);
  if (workdir != NULL) {
    git_buf root = {0};
    int error = git_repository_discover(&root, workdir, 0, NULL);
    free(workdir);
    if (error == 0) {
      git_repository *repo = NULL;
      error = git_repository_open(&repo, root.ptr);
      git_buf_free(&root);

      if (error == 0 && repo != NULL) {
        return git_repo_result_new_ok(repo);
      } else {
        return git_repo_result_new_err(error);
      }
    } else {
      return git_repo_result_new_err(error);
    }
  }

  return git_repo_result_new_err(-1);
}

gc_option_signature find_user_signature(git_repository *repo, const char *override,
                                    const char *name_env_var,
                                    const char *email_env_var) {
  if (override && strlen(override) > 0) {
    return git_comment_signature_fromstr(override);
  }

  if (name_env_var != NULL && email_env_var != NULL) {
    const char *name = getenv(name_env_var), *email = getenv(email_env_var);
    if (name && email && strlen(name) > 0 && strlen(email) > 0) {
      git_signature *sig = NULL;
      if (git_signature_now(&sig, name, email) == 0) {
        return gc_option_signature_new(sig);
      }
    }
  }

  git_config *cfg = NULL;
  if (git_repository_config(&cfg, repo) == 0) {
    git_buf name = {0}, email = {0};
    if (git_config_get_string_buf(&name, cfg, "user.name") != 0) {
      git_config_free(cfg);
      return gc_option_signature_none();
    } else if (git_config_get_string_buf(&email, cfg, "user.email") != 0) {
      git_config_free(cfg);
      return gc_option_signature_none();
    }
    git_signature *sig = NULL;
    bool success = git_signature_now(&sig, name.ptr, email.ptr) == 0;
    git_buf_dispose(&name);
    git_buf_dispose(&email);
    git_config_free(cfg);
    if (success) {
      return gc_option_signature_new(sig);
    }
  }

  return gc_option_signature_none();
}

gc_option_signature find_author_signature(git_repository *repo,
                                      const char *override) {
  return find_user_signature(repo, override, "GIT_AUTHOR_NAME",
                             "GIT_AUTHOR_EMAIL");
}

gc_option_signature find_amender_signature(git_repository *repo,
                                       const char *override) {
  return find_user_signature(repo, override, "GIT_COMMITTER_NAME",
                             "GIT_COMMITTER_EMAIL");
}

char *find_configured_editor(git_repository *repo) {
  char *editor = getenv("GIT_EDITOR");
  if (editor != NULL) {
    return strdup(editor);
  }

  git_config *cfg = NULL;
  if (git_repository_config(&cfg, repo) == 0) {
    git_buf editor_buf = {0};
    int error = git_config_get_string_buf(&editor_buf, cfg, "core.editor");
    git_config_free(cfg);
    if (!error) {
      editor = strdup(editor_buf.ptr);
      git_buf_dispose(&editor_buf);
      return editor;
    }
  }

  editor = getenv("VISUAL");
  if (editor != NULL) {
    return strdup(editor);
  }

  editor = getenv("EDITOR");
  if (editor != NULL) {
    return strdup(editor);
  }

  return strdup("vi");
}
