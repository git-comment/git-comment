#include <git2/repository.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "editor.h"
#include "repo.h"

const char *DEFAULT_EDITOR_MESSAGE =
    "\n# Enter comment content\n"
    "# Lines beginning with '#' will be stripped";

const char *COMMENT_FILENAME_TEMPLATE = "git-comment.XXXXXX";

editor_message get_message_from_editor(git_repository *repo) {
  const char *editor = find_configured_editor(repo);
  const char *basedir = git_repository_commondir(repo);

  // Create a temporary file
  size_t template_filepath_len =
      strlen(basedir) + strlen(COMMENT_FILENAME_TEMPLATE) + 2;
  char *template = calloc(1, template_filepath_len);
  snprintf(template, template_filepath_len, "%s%s", basedir,
           COMMENT_FILENAME_TEMPLATE);
  const char *filepath = mktemp(template);
  FILE *file = fopen(filepath, "w");
  if (file == NULL) {
    free(template);
    return editor_message_new_err("could not open temporary file for editing");
  }

  // Add the default message to the temp file
  size_t message_len = strlen(DEFAULT_EDITOR_MESSAGE);
  size_t written =
      fwrite(DEFAULT_EDITOR_MESSAGE, sizeof(char), message_len, file);
  fclose(file);
  if (written != message_len) {
    free(template);
    return editor_message_new_err("could write temporary file for editing");
  }

  // Launch the editor to create comment content
  size_t command_len = strlen(editor) + strlen(filepath) + 4;
  char *command = calloc(1, command_len + 1);
  snprintf(command, command_len, "%s \"%s\"", editor, filepath);
  if (system(command) != 0) {
    free(template);
    return editor_message_new_err("could not launch configured editor");
  }

  // Read comment contents
  file = fopen(filepath, "r");
  if (file == NULL) {
    free(template);
    return editor_message_new_err("could not read file containing comment");
  }

  // Get the total size
  fseek(file, 0L, SEEK_END);
  size_t file_len = ftell(file);
  rewind(file);

  // Read each line
  char *comment = calloc(1, file_len);
  char *line = NULL;
  size_t line_len = 0;
  bool has_nonempty_line = false;

  while (getline(&line, &line_len, file) != -1) {
    if (line_len > 0 && line[0] == '#') {
      // strip comment lines
      continue;
    }
    has_nonempty_line = has_nonempty_line || (line_len > 0 && line[0] != '\n');
    strncat(comment, line, line_len);
  }
  fclose(file);
  remove(filepath);
  free(template);

  if (!has_nonempty_line) {
    free(comment);
    return editor_message_new_err("aborting due to empty comment");
  }

  return editor_message_new_ok(comment);
}
