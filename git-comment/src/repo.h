#pragma once
#include "git2/types.h"
#include <git2.h>
#include <gitcomment/comment.h>
#include <option.h>
#include <result.h>

define_result_type(git_repo_result, git_repository *, int);

define_result_type(git_object_result, git_object *, int);

define_result_type(git_signature_result, git_signature *, int);

/**
 * Locate and open the git repository in the current working directory or any
 * parent dir.
 *
 * @return the repository or none if no directory contains a repository
 */
git_repo_result find_git_repository(void);

/**
 * Locate the git commit object referred to by ref
 *
 * @param repo an open repository
 * @param ref  a commitish. If empty, resolves to HEAD
 *
 * @return the object or an error code if the ref could not be resolved or is
 * resolved to an object which is not a commit
 */
git_object_result find_commit_by_ref(git_repository *repo, const char *ref);

/**
 * Find the configured author signature from sources in the following order:
 *
 * * parsing the `override` value if not NULL and not empty
 * * environment variables for GIT_AUTHOR_*
 * * repository configuration for user.name and user.email
 * * global configuration for user.name and user.email
 *
 * @param repo     an open repository
 * @param override a signature string to parse or NULL to use default values
 *
 * @return a signature if none if all sources fail to be parseable
 */
gc_option_signature find_author_signature(git_repository *repo,
                                      const char *override);

/**
 * Find the configured amender signature from sources in the following order:
 *
 * * parsing the `override` value if not NULL and not empty
 * * environment variables for GIT_COMMITTER_*
 * * repository configuration for user.name and user.email
 * * global configuration for user.name and user.email
 *
 * @param repo     an open repository
 * @param override a signature string to parse or NULL to use default values
 *
 * @return a signature if none if all sources fail to be parseable
 */
gc_option_signature find_amender_signature(git_repository *repo,
                                       const char *override);

/**
 * Determine the executable used to edit git text from (in order):
 *
 * * $GIT_EDITOR
 * * core.editor config option
 * * $VISUAL
 * * $EDITOR
 * * vi
 *
 * @param repo  an open repository
 *
 * @return a binary name or path
 */
char *find_configured_editor(git_repository *repo);
