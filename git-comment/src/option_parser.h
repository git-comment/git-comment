#pragma once
#include <gitcomment/comment.h>
#include <commander/commander.h>
#include <result.h>
#include <stdbool.h>

struct comment_opts {
  const char *delete_comment_id;
  const char *amend_comment_id;
  const char *author_signature;
  git_comment_fileref fileref;
  const char *message;
  const char *commit_ref;
  bool mark_deleted_line;
};

typedef struct {
  const char *message;
  command_t cmd;
} comment_opts_error;

define_result_type(cmdline_parse_result, command_t, comment_opts_error);

cmdline_parse_result load_cmdline_options(const char *version,
                                          struct comment_opts *opts, int argc,
                                          char **argv);
