#pragma once
#include <gitcomment/fileref.h>
#include <mpc.h>

mpc_parser_t *git_comment_fileref_parser(void);

void git_comment_parse_fileref_ast(git_comment_fileref *ref, mpc_ast_t *tree);
