#include <assert.h>
#include <git2.h>
#include <gitcomment/comment.h>
#include <mpc.h>
#include <stdlib.h>
#include <string.h>

#include "fileref.h"
#include "link.h"
#include "signature.h"

/// Parser callbacks

/**
 * Combine several 1-char strings into a single string ast node
 */
mpc_val_t *git_comment_concat(int n, mpc_val_t **xs);

/**
 * Constuct a git_comment from ast nodes
 *
 * @return a comment or NULL upon failure
 */
mpc_val_t *git_comment_ctor(int n, mpc_val_t **input);

void git_comment_init() { git_libgit2_init(); }

gc_option_comment git_comment_fromstr(const char *in) {
  mpc_parser_t *person = mpc_new("person");
  mpc_parser_t *oid = mpc_new("oid");
  mpc_parser_t *commit = mpc_new("commit");
  mpc_parser_t *author = mpc_new("author");
  mpc_parser_t *amender = mpc_new("amender");
  mpc_parser_t *links = mpc_new("links");
  mpc_parser_t *link = mpc_new("link");
  mpc_parser_t *file = git_comment_fileref_parser();

  mpca_lang(MPCA_LANG_WHITESPACE_SENSITIVE,
            "person   : /.+/;"
            "oid      : /[0-9a-f]+/;"
            "commit   : \"commit\" ' ' <oid> '\n';"
            "author   : \"author\" ' ' <person> '\n';"
            "amender  : \"amender\" ' ' <person> '\n';"
            "links    : \"links:\" '\n' (<link>)+;"
            "link     : '*' /([^<>\\s]+|\\s)+/ ('<' /[^<>]+/ '>') '\n';",
            person, oid, commit, author, amender, links, link, NULL);

  mpc_parser_t *comment = mpc_and(
      7, git_comment_ctor, commit, file, author, amender, mpca_maybe(links),
      mpc_newline(), mpc_many1(git_comment_concat, mpc_any()), free, free, free,
      free, free, free, free, free);

  gc_option_comment output;
  mpc_result_t result;
  if (mpc_parse("<git-comment>", in, comment, &result)) {
    git_comment *comment = result.output;
    if (comment == NULL) {
      output = gc_option_comment_none();
    } else {
      output = gc_option_comment_new(comment);
    }
  } else {
    mpc_err_print(result.error);
    mpc_err_delete(result.error);
    output = gc_option_comment_none();
  }

  mpc_cleanup(8, person, commit, oid, file, author, links, link, comment);

  return output;
}

char *git_comment_commit_tostr(git_oid *commit) {
  char *serialized = calloc(GIT_OID_HEXSZ + 1, 1);
  git_oid_tostr(serialized, GIT_OID_HEXSZ + 1, commit);
  return serialized;
}

gc_option_str git_comment_tostr(git_comment *comment) {
  if (!git_comment_validate_signature(comment->author) ||
      !git_comment_validate_signature(comment->amender) || !comment->content ||
      strlen(comment->content) == 0 || comment->fileref.line < 0 ||
      (comment->fileref.path && strlen(comment->fileref.path) > 0 &&
       comment->fileref.line == 0)) {
    return gc_option_str_none();
  }
  char *commit = git_comment_commit_tostr(&comment->commit);
  if (strlen(commit) == 0) {
    free(commit);
    return gc_option_str_none();
  }
  char *fileref = git_comment_fileref_tostr(&comment->fileref);
  char *author = git_comment_signature_tostr(comment->author);
  char *amender = git_comment_signature_tostr(comment->amender);
  char *links = git_comment_links_tostr(comment->link_count, comment->links);
  const char *format = "commit %s\nfile %s\nauthor %samender %s%s\n%s";
  char *result;
  asprintf(&result, format, commit, fileref, author, amender, links,
           comment->content);
  free(commit);
  free(fileref);
  free(links);
  free(author);
  free(amender);
  return gc_option_str_new(result);
}

bool git_comment_add_link(git_comment *comment, char *id, char *url) {
  if (id && url && strlen(id) > 0 && strlen(url) > 0) {
    comment->links = realloc(comment->links, comment->link_count + 1);
    if (comment->links == NULL) {
      return false;
    }
    comment->links[comment->link_count].id = id;
    comment->links[comment->link_count].url = url;
    comment->link_count++;
    return true;
  }
  return false;
}

void git_comment_free(git_comment *comment) {
  if (comment->author) {
    git_signature_free(comment->author);
    comment->author = NULL;
  }
  if (comment->amender) {
    git_signature_free(comment->amender);
    comment->amender = NULL;
  }
  if (comment->content) {
    free(comment->content);
    comment->content = NULL;
  }
  if (comment->fileref.path) {
    free(comment->fileref.path);
    comment->fileref.path = NULL;
  }
  for (int i = 0; i < comment->link_count; i++) {
    free(comment->links[i].id);
    free(comment->links[i].url);
  }
  comment->link_count = 0;
}

mpc_val_t *git_comment_concat(int n, mpc_val_t **xs) {
  char *x = calloc(n + 1, 1);
  for (int i = 0; i < n; i++) {
    x[i] = *(const char *)xs[i];
    free(xs[i]);
  }
  return mpc_ast_new("content", x);
}

mpc_val_t *git_comment_ctor(int n, mpc_val_t **input) {
  assert(n == 7);
  git_comment *comment = calloc(1, sizeof(git_comment));

  mpc_ast_t *commit = input[0];
  mpc_ast_t *oid = mpc_ast_get_child(commit, "oid|regex");
  bool parsed_oid = git_oid_fromstr(&comment->commit, oid->contents) == 0;

  mpc_ast_t *file = input[1];
  git_comment_parse_fileref_ast(&comment->fileref, file);

  mpc_ast_t *author = input[2];
  mpc_ast_t *authorname = mpc_ast_get_child(author, "person|regex");
  bool parsed_author =
      git_signature_from_buffer(&comment->author, authorname->contents) == 0;

  mpc_ast_t *amender = input[3];
  mpc_ast_t *amendername = mpc_ast_get_child(amender, "person|regex");
  bool parsed_amender =
      git_signature_from_buffer(&comment->amender, amendername->contents) == 0;

  mpc_ast_t *links = input[4];
  comment->link_count = git_comment_parse_links_ast(&comment->links, links);

  // 5 is a newline

  mpc_ast_t *content = input[6];
  comment->content = strdup(content->contents);

  for (int i = 0; i < n; i++) {
    free(input[i]);
  }

  bool success = parsed_oid && parsed_author && parsed_amender;

  if (success) {
    return comment;
  } else {
    git_comment_free(comment);
    free(comment);
    return NULL;
  }
}
