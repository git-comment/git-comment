#include <errno.h>
#include <string.h>

#include "fileref.h"

mpc_parser_t *git_comment_fileref_parser() {
  mpc_parser_t *file = mpc_new("file");
  mpca_lang(MPCA_LANG_WHITESPACE_SENSITIVE,
            "file : \"file\" (' ' /[^:]+/ ':' /[0-9]+/)? '\n';", file, NULL);

  return file;
}

void git_comment_parse_fileref_ast(git_comment_fileref *ref, mpc_ast_t *tree) {
  if (tree->children_num > 4) {
    ref->path = strdup(tree->children[2]->contents);
    char *lineno = tree->children[4]->contents;
    char *end = lineno + strlen(lineno);
    errno = 0;
    ref->line = strtol(lineno, &end, 10);
    if (errno != 0) {
      free(ref->path);
      memset(ref, 0, sizeof(git_comment_fileref));
    }
  }
}

char *git_comment_fileref_tostr(git_comment_fileref *ref) {
  char *serialized = NULL;
  if (ref->path && strlen(ref->path) > 0 && ref->line > 0) {
    asprintf(&serialized, "%s:%lu", ref->path, ref->line);
  } else {
    serialized = strdup("");
  }
  return serialized;
}

gc_option_fileref git_comment_fileref_fromstr(const char *in) {
  mpc_parser_t *file = mpc_new("file");
  mpca_lang(MPCA_LANG_WHITESPACE_SENSITIVE,
            "file : /^([^:]|\\s)+/ ':' /[0-9]+$/;", file, NULL);

  mpc_result_t result;
  int success = mpc_parse("<fileref>", in, file, &result);
  mpc_cleanup(1, file);
  if (success) {
    git_comment_fileref ref;
    mpc_ast_t *tree = result.output;
    if (tree->children_num == 3) {
      ref.path = strdup(tree->children[0]->contents);
      char *lineno = tree->children[2]->contents;
      char *end = lineno + strlen(lineno);
      errno = 0;
      ref.line = strtol(lineno, &end, 10);
      mpc_ast_delete(tree);
      if (errno != 0 || ref.line < 1) {
        free(ref.path);
      } else {
        return gc_option_fileref_new(ref);
      }
    } else {
      mpc_ast_delete(tree);
    }
  } else {
    mpc_err_delete(result.error);
  }

  return gc_option_fileref_none();
}
