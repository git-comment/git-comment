#include <git2/signature.h>
#include <stdio.h>
#include <string.h>

#include "signature.h"

bool git_comment_validate_signature(git_signature *sig) {
  return (sig && sig->name && sig->email && sig->when.offset &&
          strlen(sig->name) > 0 && strlen(sig->email) > 0);
}

char *git_comment_signature_tostr(git_signature *sig) {
  if (!git_comment_validate_signature(sig)) {
    return strdup("");
  }
  int offset = sig->when.offset;
  char sign = (offset < 0 || sig->when.sign == '-') ? '-' : '+';

  if (offset < 0)
    offset = -offset;

  int hours = offset / 60;
  int mins = offset % 60;

  char *serialized = NULL;
  asprintf(&serialized, "%s <%s> %u %c%02d%02d\n", sig->name, sig->email,
           (unsigned)sig->when.time, sign, hours, mins);
  return serialized;
}

gc_option_signature git_comment_signature_fromstr(const char *in) {
  git_signature *sig = NULL;
  if (git_signature_from_buffer(&sig, in) == 0) {
    if (strlen(sig->name) == 0 || strlen(sig->email) == 0) {
      // discard partial signature as invalid. Its possible to generate these
      // via git_signature_from_buffer() using solely an email, but then they
      // are invalid for further signature operations.
      git_signature_free(sig);
      return gc_option_signature_none();
    } else if (sig->when.time == 0) {
      // signatures in the format 'A Name <email@example.com>' do not come with
      // a timestamp, so we'll add one for convenience.
      git_signature *now = NULL;
      if (git_signature_now(&now, sig->name, sig->email) == 0) {
        git_signature_free(sig);
        sig = now;
      }
    }
    return gc_option_signature_new(sig);
  }
  return gc_option_signature_none();
}
