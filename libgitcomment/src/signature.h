#pragma once
#include <gitcomment/signature.h>

bool git_comment_validate_signature(git_signature *sig);
