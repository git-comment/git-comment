#pragma once
#include <mpc.h>
#include <gitcomment/link.h>

unsigned int git_comment_parse_links_ast(git_comment_link **links, mpc_ast_t *ast);
