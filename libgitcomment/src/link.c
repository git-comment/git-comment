#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "link.h"

char *git_comment_links_tostr(unsigned int link_count, git_comment_link *links) {
  if (link_count == 0 || !links) {
    return strdup("");
  }
  const char *title = "links:\n";
  int len = 1, i = 0;
  git_comment_link link = *links;
  for (i = 0, link = links[0]; i < link_count; link = links[++i]) {
    len += strlen(link.id) + strlen(link.url) + 6; // '* %s <%s>\n'
  }
  char *serialized = calloc(strlen(title) + len, 1);
  strcat(serialized, title);
  for (i = 0, link = links[0]; i < link_count; link = links[++i]) {
    strcat(serialized, "* ");
    strcat(serialized, link.id);
    strcat(serialized, " <");
    strcat(serialized, link.url);
    strcat(serialized, ">\n");
  }

  return serialized;
}

unsigned int git_comment_parse_links_ast(git_comment_link **links, mpc_ast_t *ast) {
  int link_count = 0;
  if (ast != NULL) {
    *links = calloc(1, sizeof(git_comment_link));

    for (int i = 0; i < ast->children_num; i++) {
      mpc_ast_t *link = ast->children[i];

      if (link && link->tag && strcmp("link|>", link->tag) == 0) {
        assert(link->children_num == 6);
        link_count++;

        // Expand to add more links
        *links = realloc(*links, sizeof(git_comment_link) * link_count);

        char *id = strdup(link->children[1]->contents);
        char *end = id + strlen(id) - 1;
        // Strip leading spaces
        while (id < end && *id == ' ') {
          id++;
        }

        // Strip trailing spaces
        while (end > id && *end == ' ') {
          end--;
        }
        end[1] = '\0';

        (*links)[link_count - 1].id = id;
        (*links)[link_count - 1].url = strdup(link->children[3]->contents);
      }
    }
  }
  return link_count;
}


