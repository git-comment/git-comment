#pragma once
#include <option.h>

typedef struct {
  char *path;
  unsigned long line;
} git_comment_fileref;

define_option_type(gc_option_fileref, git_comment_fileref);

gc_option_fileref git_comment_fileref_fromstr(const char *in);

char *git_comment_fileref_tostr(git_comment_fileref *ref);
