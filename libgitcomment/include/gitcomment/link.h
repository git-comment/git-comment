#pragma once

typedef struct {
  char *id;
  char *url;
} git_comment_link;

char *git_comment_links_tostr(unsigned int link_count, git_comment_link *links);
