#pragma once

#include <git2/types.h>
#include <option.h>
#include <stdbool.h>

#include "fileref.h"
#include "link.h"
#include "signature.h"

typedef struct {
  git_oid commit;
  git_comment_fileref fileref;

  git_signature *author;
  git_signature *amender;

  git_comment_link *links;
  unsigned int link_count;
  char *content;
} git_comment;

define_option_type(gc_option_comment, git_comment *);

define_option_type(gc_option_str, char *);

void git_comment_init(void);

gc_option_comment git_comment_fromstr(const char *in);

gc_option_str git_comment_tostr(git_comment *in);

bool git_comment_add_link(git_comment *comment, char *id, char *url);

void git_comment_free(git_comment *comment);
