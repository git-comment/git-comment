#pragma once
#include <git2/types.h>
#include <option.h>
#include <stdbool.h>

define_option_type(gc_option_signature, git_signature *);

gc_option_signature git_comment_signature_fromstr(const char *in);

char *git_comment_signature_tostr(git_signature *sig);
