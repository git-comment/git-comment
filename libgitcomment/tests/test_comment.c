#include <string.h>
#include <suspect.h>

#include <git2/signature.h>
#include <gitcomment/comment.h>

#define suspect_oid_eq(oid, strvalue)                                          \
  char oid_fmt[41];                                                            \
  memset(oid_fmt, 0, 41);                                                      \
  git_oid_fmt(oid_fmt, &oid);                                                  \
  suspect_streq(oid_fmt, strvalue);

void test_parse_comment() {
  const char *input =
      "commit 0155eb4229851634a0f03eb265b69f5a2d56f341\n"
      "file src/parser.c:12\n"
      "author Daniel Pri <dan@example.com> 1243040974 -0900\n"
      "amender Daniel Pro <dano@example.com> 1243040964 +0800\n"
      "\n"
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.";
  gc_option_comment result = git_comment_fromstr(input);
  suspect_true(gc_option_comment_is_some(result));
  if (!result.is_some) {
    return;
  }

  suspect_oid_eq(result.value->commit,
                 "0155eb4229851634a0f03eb265b69f5a2d56f341");

  suspect_streq(result.value->fileref.path, "src/parser.c");
  suspect_eq(result.value->fileref.line, 12);

  suspect_streq(result.value->author->name, "Daniel Pri");
  suspect_streq(result.value->author->email, "dan@example.com");
  suspect_eq(result.value->author->when.time, 1243040974);
  suspect_eq(result.value->author->when.offset, -540);

  suspect_streq(result.value->amender->name, "Daniel Pro");
  suspect_streq(result.value->amender->email, "dano@example.com");
  suspect_eq(result.value->amender->when.time, 1243040964);
  suspect_eq(result.value->amender->when.offset, 480);

  suspect_streq(
      result.value->content,
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.");
}

void test_parse_comment_without_file() {
  const char *input =
      "commit 0155eb4229851634a0f03eb265b69f5a2d56f341\n"
      "file\n"
      "author Daniel Pri <dan@example.com> 1243040974 -0900\n"
      "amender Daniel Pro <dano@example.com> 1243040964 +0800\n"
      "\n"
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.";
  gc_option_comment result = git_comment_fromstr(input);
  suspect_true(gc_option_comment_is_some(result));
  if (!result.is_some) {
    return;
  }

  suspect_oid_eq(result.value->commit,
                 "0155eb4229851634a0f03eb265b69f5a2d56f341");

  suspect_true(result.value->fileref.path == NULL);
  suspect_eq(result.value->fileref.line, 0);

  suspect_streq(result.value->author->name, "Daniel Pri");
  suspect_eq(result.value->author->when.time, 1243040974);
  suspect_eq(result.value->author->when.offset, -540);

  suspect_streq(result.value->amender->name, "Daniel Pro");
  suspect_eq(result.value->amender->when.time, 1243040964);
  suspect_eq(result.value->amender->when.offset, 480);

  suspect_streq(
      result.value->content,
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.");
}
void test_parse_comment_with_links() {
  const char *input =
      "commit 0155eb4229851634a0f03eb265b69f5a2d56f341\n"
      "file src/parser.c:12\n"
      "author Daniel Pri <dan@example.com> 1243040974 -0900\n"
      "amender Daniel Pro <dano@example.com> 1243040964 +0800\n"
      "links:\n"
      "* sync url <https://api.example.com/comments/123>\n"
      "* rfc <example.com/ietf/rfcs/29>\n"
      "\n"
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.";
  gc_option_comment result = git_comment_fromstr(input);
  suspect_true(gc_option_comment_is_some(result));
  if (!result.is_some) {
    return;
  }

  suspect_oid_eq(result.value->commit,
                 "0155eb4229851634a0f03eb265b69f5a2d56f341");

  suspect_streq(result.value->fileref.path, "src/parser.c");
  suspect_eq(result.value->fileref.line, 12);

  suspect_streq(result.value->author->name, "Daniel Pri");
  suspect_eq(result.value->author->when.time, 1243040974);
  suspect_eq(result.value->author->when.offset, -540);

  suspect_streq(result.value->amender->name, "Daniel Pro");
  suspect_eq(result.value->amender->when.time, 1243040964);
  suspect_eq(result.value->amender->when.offset, 480);

  suspect_eq(result.value->link_count, 2);
  suspect_streq(result.value->links[0].id, "sync url");
  suspect_streq(result.value->links[0].url,
                "https://api.example.com/comments/123");
  suspect_streq(result.value->links[1].id, "rfc");
  suspect_streq(result.value->links[1].url, "example.com/ietf/rfcs/29");

  suspect_streq(
      result.value->content,
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.");
}

void test_parse_comment_with_content_links() {
  const char *input =
      "commit 0155eb4229851634a0f03eb265b69f5a2d56f341\n"
      "file src/parser.c:12\n"
      "author Daniel Pri <dan@example.com> 1243040974 -0900\n"
      "amender Daniel Pro <dano@example.com> 1243040964 +0800\n"
      "\n"
      "links:\n"
      "* sync url <https://api.example.com/comments/123>\n"
      "* rfc <example.com/ietf/rfcs/29>\n"
      "\n"
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.";
  gc_option_comment result = git_comment_fromstr(input);
  suspect_true(gc_option_comment_is_some(result));
  if (!result.is_some) {
    return;
  }

  suspect_oid_eq(result.value->commit,
                 "0155eb4229851634a0f03eb265b69f5a2d56f341");

  suspect_streq(result.value->fileref.path, "src/parser.c");
  suspect_eq(result.value->fileref.line, 12);

  suspect_streq(result.value->author->name, "Daniel Pri");
  suspect_eq(result.value->author->when.time, 1243040974);
  suspect_eq(result.value->author->when.offset, -540);

  suspect_streq(result.value->amender->name, "Daniel Pro");
  suspect_eq(result.value->amender->when.time, 1243040964);
  suspect_eq(result.value->amender->when.offset, 480);

  suspect_eq(result.value->link_count, 0);

  suspect_streq(
      result.value->content,
      "links:\n"
      "* sync url <https://api.example.com/comments/123>\n"
      "* rfc <example.com/ietf/rfcs/29>\n"
      "\n"
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.");
}

void test_serialize_comment() {
  git_comment *comment = calloc(1, sizeof(git_comment));
  git_oid_fromstr(&comment->commit, "0155eb4229851634a0f03eb265b69f5a2d56f341");
  git_signature_from_buffer(&comment->author,
                            "Emily <em@example.com> 1243040974 -0900");
  git_signature_from_buffer(&comment->amender,
                            "Daniel Pro <dano@example.com> 1243040964 +0800");
  comment->fileref.path = strdup("src/parser.c");
  comment->fileref.line = 14;
  comment->content = strdup(
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.");

  gc_option_str serialized = git_comment_tostr(comment);
  suspect_true(gc_option_str_is_some(serialized));

  char *expected =
      "commit 0155eb4229851634a0f03eb265b69f5a2d56f341\n"
      "file src/parser.c:14\n"
      "author Emily <em@example.com> 1243040974 -0900\n"
      "amender Daniel Pro <dano@example.com> 1243040964 +0800\n"
      "\n"
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.";
  suspect_streq(serialized.value, expected);

  git_comment_free(comment);
  free(comment);
  free(serialized.value);
}

void test_serialize_invalid_comment() {
  git_comment *comment = calloc(1, sizeof(git_comment));
  git_oid_fromstr(&comment->commit, "0155eb4229851634a0f03eb265b69f5a2d56f341");
  comment->fileref.path = strdup("src/parser.c");
  comment->fileref.line = 14;
  comment->content = strdup(
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.");

  gc_option_str serialized = git_comment_tostr(comment);
  suspect_true(gc_option_str_is_none(serialized));

  git_comment_free(comment);
  free(comment);
}

void test_serialize_comment_with_links() {
  git_comment *comment = calloc(1, sizeof(git_comment));
  git_oid_fromstr(&comment->commit, "0155eb4229851634a0f03eb265b69f5a2d56f341");
  git_signature_from_buffer(&comment->author,
                            "Emily <em@example.com> 1243040974 -0900");
  git_signature_from_buffer(&comment->amender,
                            "Daniel Pro <dano@example.com> 1243040964 +0800");
  comment->fileref.path = strdup("src/parser.c");
  comment->fileref.line = 14;
  comment->content = strdup(
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.");
  comment->links = calloc(1, sizeof(git_comment_link));
  comment->links[0].id = strdup("sync url");
  comment->links[0].url = strdup("https://example.com/api/comments/12222");
  comment->link_count = 1;

  gc_option_str serialized = git_comment_tostr(comment);
  suspect_true(gc_option_str_is_some(serialized));

  char *expected =
      "commit 0155eb4229851634a0f03eb265b69f5a2d56f341\n"
      "file src/parser.c:14\n"
      "author Emily <em@example.com> 1243040974 -0900\n"
      "amender Daniel Pro <dano@example.com> 1243040964 +0800\n"
      "links:\n"
      "* sync url <https://example.com/api/comments/12222>\n"
      "\n"
      "Too many levels of indentation here.\n"
      "\n"
      "Consider updating the linter as well as a part of this changeset.";
  suspect_streq(serialized.value, expected);

  git_comment_free(comment);
  free(comment);
  free(serialized.value);
}

void test_suite_comment() {
  suspect_suite("comments");

  suspect_run_test(test_parse_comment);
  suspect_run_test(test_parse_comment_without_file);
  suspect_run_test(test_parse_comment_with_links);
  suspect_run_test(test_parse_comment_with_content_links);

  suspect_run_test(test_serialize_comment);
  suspect_run_test(test_serialize_invalid_comment);
  suspect_run_test(test_serialize_comment_with_links);
}
