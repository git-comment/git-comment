#define SUSPECT_HANDLE_CRASH
#define SUSPECT_IMPLEMENTATION
#include <suspect.h>

#include <gitcomment/comment.h>

void test_suite_comment();
void test_suite_fileref();
void test_suite_links();
void test_suite_signature();

int main(int argc, char **argv) {
  suspect_setup(argc, argv);

  git_comment_init();

  test_suite_comment();
  test_suite_fileref();
  test_suite_links();
  test_suite_signature();

  suspect_summarize();
  return suspect_exit_code();
}
