#include <suspect.h>

#include <gitcomment/comment.h>

void test_signature_fromstr() {
  gc_option_signature result = git_comment_signature_fromstr(
      "Katie Doe <katie@example.com> 1243040974 -0900");

  suspect_true(gc_option_signature_is_some(result));

  git_signature *sig = result.value;

  suspect_streq(sig->name, "Katie Doe");
  suspect_streq(sig->email, "katie@example.com");
  suspect_eq(sig->when.time, 1243040974);
  suspect_eq(sig->when.offset, -540);
}

void test_parse_name_email_signature() {
  gc_option_signature result =
      git_comment_signature_fromstr("Katie Doe <katie@example.com>");

  suspect_true(gc_option_signature_is_some(result));

  git_signature *sig = result.value;

  suspect_streq(sig->name, "Katie Doe");
  suspect_streq(sig->email, "katie@example.com");
  suspect_true(sig->when.time != 0);
}

void test_parse_name_only_signature() {
  gc_option_signature result = git_comment_signature_fromstr("Katie Doe");

  suspect_true(gc_option_signature_is_none(result));
}

void test_parse_email_only_signature() {
  gc_option_signature result =
      git_comment_signature_fromstr("<katie@example.com>");

  suspect_true(gc_option_signature_is_none(result));
}

void test_suite_signature() {
  suspect_suite("signatures");
  suspect_run_test(test_signature_fromstr);
  suspect_run_test(test_parse_name_email_signature);
  suspect_run_test(test_parse_name_only_signature);
  suspect_run_test(test_parse_email_only_signature);
}
