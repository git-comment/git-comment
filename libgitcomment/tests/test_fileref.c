#include <gitcomment/fileref.h>
#include <stdlib.h>
#include <suspect.h>

#define suspect_is_none(value) suspect_true(gc_option_fileref_is_none(value));

void test_fileref_fromstr() {
  gc_option_fileref ref = git_comment_fileref_fromstr("some path:23");
  suspect_true(gc_option_fileref_is_some(ref));
  suspect_streq("some path", ref.value.path);
  suspect_eq_fmt(23L, ref.value.line, "%lu");

  free(ref.value.path);
}

void test_invalid_fileref_fromstr() {
  suspect_is_none(git_comment_fileref_fromstr(""));
  suspect_is_none(git_comment_fileref_fromstr(""));
  suspect_is_none(git_comment_fileref_fromstr(":"));
  suspect_is_none(git_comment_fileref_fromstr(":89"));
  suspect_is_none(git_comment_fileref_fromstr("f:n"));
  suspect_is_none(git_comment_fileref_fromstr("f:-12"));
  suspect_is_none(git_comment_fileref_fromstr("f:0"));
  suspect_is_none(git_comment_fileref_fromstr("a path: 8"));
  suspect_is_none(git_comment_fileref_fromstr("a path:8a"));
}

void test_invalid_fileref_tostr() {
  git_comment_fileref ref = {0};
  suspect_streq("", git_comment_fileref_tostr(&ref));
  ref = (git_comment_fileref) {"path/to/file", 0};
  suspect_streq("", git_comment_fileref_tostr(&ref));
  ref = (git_comment_fileref) {"", 733};
  suspect_streq("", git_comment_fileref_tostr(&ref));
}

void test_fileref_tostr() {
  git_comment_fileref ref = {"path to/a/file.txt", 801};
  suspect_streq("path to/a/file.txt:801", git_comment_fileref_tostr(&ref));
}

void test_suite_fileref() {
  suspect_suite("filerefs");
  suspect_run_test(test_fileref_fromstr);
  suspect_run_test(test_invalid_fileref_fromstr);
  suspect_run_test(test_invalid_fileref_tostr);
  suspect_run_test(test_fileref_tostr);
}
