#include <gitcomment/link.h>
#include <stdlib.h>
#include <suspect.h>

void test_links_tostr() {
  git_comment_link links[2] = {
      {"RFC", "https://example.com/rfcs/1422"},
      {"sync url", "api.example.com/comments/ffa9"},
  };

  char *serialized = git_comment_links_tostr(2, links);
  char *expected = "links:\n"
                   "* RFC <https://example.com/rfcs/1422>\n"
                   "* sync url <api.example.com/comments/ffa9>\n";
  suspect_streq(expected, serialized);
  free(serialized);
}

void test_suite_links() {
  suspect_suite("links");
  suspect_run_test(test_links_tostr);
}
