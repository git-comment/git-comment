#pragma once
#include <assert.h>
#include <stdbool.h>

/**
 * Define a new optional type and helper functions
 *
 * @param name      The name of the optional type struct
 * @param some_type The type signature of the value
 *
 * ```
 * #include <stdio.h>
 *
 * define_option_type(optional_str,char *)
 *
 * optional_str read_input() {
 *   return optional_str_new("Some text!");
 * }
 *
 * optional_str read_empty() {
 *   return optional_str_none();
 * }
 *
 * int main() {
 *   optional_str result = read_input();
 *
 *   // check value:
 *   if (optional_str_is_some(result)) {
 *     printf("Received text: '%s'\n", optional_str_unwrap(result));
 *   }
 *   // handle none case with default value:
 *   printf("Received text: '%s'\n", optional_str_unwrap_or(result, "(empty)"));
 * }
 * ```
 */
#define define_option_type(name, some_type)                                    \
  typedef struct {                                                             \
    bool is_some;                                                              \
    some_type value;                                                           \
  } name;                                                                      \
  /** Check if value is set */                                                 \
  static inline bool name##_is_some(name o) { return o.is_some; }              \
  /** Check if value is not set */                                             \
  static inline bool name##_is_none(name o) { return !o.is_some; }             \
  /** Extract value */                                                         \
  static inline some_type name##_unwrap(name o) {                              \
    assert(o.is_some);                                                         \
    return o.value;                                                            \
  }                                                                            \
  /** Extract value or default value */                                        \
  static inline some_type name##_unwrap_or(name o, some_type default_value) {  \
    return o.is_some ? o.value : default_value;                                \
  }                                                                            \
  /** Return a new some optional */                                            \
  static inline name name##_new(some_type value) {                             \
    return (name){.is_some = true, .value = value};                            \
  }                                                                            \
  /** Return a new none optional */                                            \
  static inline name name##_none(void) { return (name){.is_some = false}; }
