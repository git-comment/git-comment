#pragma once
#include <assert.h>
#include <stdbool.h>

typedef enum {
  result_type_err = 0,
  result_type_ok,
} result_type;

/**
 * Define a new result type and helper functions
 *
 * @param name     The name of the result type struct
 * @param ok_type  The type signature of the success value
 * @param err_type The type signature of the failure value
 *
 * ```
 * #include <stdio.h>
 *
 * typedef enum {
 *   code_notfound,
 *   code_invalid,
 *   code_unknown,
 * } code;
 *
 * define_result_type(str_result,char *,code)
 *
 * str_result read_input() {
 *   return str_result_new_ok("Some text!");
 * }
 *
 * str_result read_bad_input() {
 *   return str_result_new_err(code_notfound);
 * }
 *
 * int main() {
 *   str_result result = read_input();
 *   if (str_result_is_ok(result)) {
 *     printf("Received text: '%s'\n", str_result_value(result));
 *   } else {
 *     printf("Received an error: %d\n", str_result_error(result));
 *   }
 * }
 * ```
 */
#define define_result_type(name, ok_type, err_type)                            \
  typedef struct {                                                             \
    result_type type;                                                          \
    union {                                                                    \
      ok_type value;                                                           \
      err_type error;                                                          \
    };                                                                         \
  } name;                                                                      \
  /** Check if the result succeeded */                                         \
  static inline bool name##_is_ok(name r) { return r.type == result_type_ok; } \
  /** Check if the result failed */                                            \
  static inline bool name##_is_err(name r) {                                   \
    return r.type == result_type_err;                                          \
  }                                                                            \
  /** Extract success value */                                                 \
  static inline ok_type name##_value(name r) {                                 \
    assert(name##_is_ok(r));                                                   \
    return r.value;                                                            \
  }                                                                            \
  /** Extract error value */                                                   \
  static inline err_type name##_err(name r) {                                  \
    assert(name##_is_err(r));                                                  \
    return r.error;                                                            \
  }                                                                            \
  /** Return a new successful result */                                        \
  static inline name name##_new_ok(ok_type value) {                            \
    return (name){.type = result_type_ok, .value = value};                     \
  }                                                                            \
  /** Return a new failed result */                                            \
  static inline name name##_new_err(err_type err) {                            \
    return (name){.type = result_type_err, .error = err};                      \
  }
