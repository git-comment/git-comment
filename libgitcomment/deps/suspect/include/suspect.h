#pragma once
#if defined(__linux__)
#define _POSIX_C_SOURCE 200809L
#endif
#include <stdbool.h>
#include <string.h>

/**
 * Initialize the test run.
 */
void suspect_setup(int argc, char **argv);

/**
 * Initialize the test suite. Aggregates all subsequent results under a common
 * heading until the next test suite grouping.
 *
 * @param suite_name The suite name
 */
void suspect_suite(const char *suite_name);

/**
 * Ends the test run, printing the results
 */
void suspect_summarize(void);

/**
 * An exit code value based on the results of the test run
 *
 * @return 0 if all tests passed / skipped, 1 if any test failed
 */
int suspect_exit_code(void);

/**
 * Add a test assertion that result is true, otherwise failing the test with a
 * formatted message. This function can be wrapped to generate custom
 * assertions.
 *
 *     #define assert_is_2(value) \
 *       __suspect_assert(value == 2, __func__, __FILE__, __LINE__, \
 *                        "value was %d not 2", value)
 *
 * @param result The value which should be true
 * @param func The test function name
 * @param file The test function file
 * @param lineno The test assertion line number
 * @param format Failure message format
 * @param ... Failure message arguments
 */
void __suspect_assert(bool result, const char *func, const char *file,
                      long lineno, const char *format, ...)
    __attribute__((format(printf, 5, 6)));

void __suspect_assert_streq(const char *val1, const char *val2,
                            const char *func, const char *file, long lineno);

/**
 * Run a test, which may contain one or many assertions. A test without
 * assertions is considered a skipped test.
 */
void __suspect_run_test(void (*test_fn)(void), const char *func,
                        const char *file);

#define suspect_run_test(func) __suspect_run_test(func, #func, __FILE__)

#define suspect_true(value)                                                    \
  __suspect_assert(value == 1, __func__, __FILE__, __LINE__, "value is false")

#define suspect_false(value)                                                   \
  __suspect_assert(value == 0, __func__, __FILE__, __LINE__, "value is true")

#define suspect_eq(val1, val2)                                                 \
  __suspect_assert(val1 == val2, __func__, __FILE__, __LINE__, "%f != %f",     \
                   (double)val1, (double)val2)

#define suspect_eq_fmt(val1, val2, fmt)                                        \
  __suspect_assert(val1 == val2, __func__, __FILE__, __LINE__, fmt " != " fmt, \
                   val1, val2)

#define suspect_streq(val1, val2)                                              \
  __suspect_assert_streq(val1, val2, __func__, __FILE__, __LINE__)

#ifdef SUSPECT_IMPLEMENTATION
#include <stdarg.h> // for va_list() and friends
#include <stdio.h>  // for fprintf(), fileno(), popen()

#ifdef SUSPECT_HANDLE_CRASH
#if defined(_WIN32)
#error "Crash handling is only supported on POSIX platforms at this time"
#endif
#include <execinfo.h> // for backtrace(), backtrace_symbols_fd()
#include <signal.h>   // for signal(), signal names
#include <unistd.h>   // for STDERR_FILENO
#endif

#define STRLEN_MAX 65536

typedef struct {
  int assertions_total;
  int assertions_failed;
} suspect_test;

typedef struct {
  const char *name;
  int assertions_total;
  int tests_total;
  int tests_passed;
  int tests_failed;
  int tests_skipped;
} suspect_suite_stats;

typedef struct {
  suspect_suite_stats current_suite;
  suspect_suite_stats aggregate;
  suspect_test current;
  short started;
  char **specs;
  int spec_count;
} suspect_stats;

#define __suspect_suite_tally()                                                \
  suspect_testrun.aggregate.assertions_total +=                                \
      suspect_testrun.current_suite.assertions_total;                          \
  suspect_testrun.aggregate.tests_skipped +=                                   \
      suspect_testrun.current_suite.tests_skipped;                             \
  suspect_testrun.aggregate.tests_failed +=                                    \
      suspect_testrun.current_suite.tests_failed;                              \
  suspect_testrun.aggregate.tests_passed +=                                    \
      suspect_testrun.current_suite.tests_passed;                              \
  suspect_testrun.aggregate.tests_total +=                                     \
      suspect_testrun.current_suite.tests_total;                               \
  suspect_testrun.current_suite = (suspect_suite_stats){};

static suspect_stats suspect_testrun = {};

int suspect_exit_code() {
  return suspect_testrun.aggregate.tests_failed == 0 ? 0 : 1;
}

#ifdef SUSPECT_HANDLE_CRASH
void (*suspect_prev_handlers[12])(int);
void suspect_crash_handler(int sig) {
  suspect_testrun.current_suite.tests_failed++;
  fprintf(stdout, "!");
  suspect_summarize();
  fflush(stdout);
  void *frames[10];
  size_t count = backtrace(frames, 10);
  fprintf(stderr, "\nTests crashed with signal %d:\n", sig);
  backtrace_symbols_fd(frames, count, fileno(stderr));
  if (suspect_prev_handlers[sig]) {
    signal(sig, suspect_prev_handlers[sig]);
    suspect_prev_handlers[sig](sig);
  } else {
    signal(sig, NULL);
  }
}
#endif

void suspect_setup(int argc, char **argv) {
  suspect_testrun = (suspect_stats){};
  if (argc > 1) {
    suspect_testrun.specs = &argv[1];
    suspect_testrun.spec_count = argc - 1;
  }
#ifdef SUSPECT_HANDLE_CRASH
  int signals[] = {SIGABRT, SIGFPE, SIGILL, SIGSEGV, SIGTRAP, 0};
  for (int i = 0, sig = signals[i]; sig != 0; sig = signals[++i]) {
    suspect_prev_handlers[sig] = signal(sig, suspect_crash_handler);
  }
#endif
}

void suspect_suite_summarize() {
  if (suspect_testrun.current_suite.tests_total == 0) {
    return;
  }
  fprintf(stdout,
          "\n  %d tests - %d passed, %d failed, %d skipped (%d assertions)\n",
          suspect_testrun.current_suite.tests_total,
          suspect_testrun.current_suite.tests_passed,
          suspect_testrun.current_suite.tests_failed,
          suspect_testrun.current_suite.tests_skipped,
          suspect_testrun.current_suite.assertions_total);
  __suspect_suite_tally();
}

void suspect_suite(const char *suite_name) {
  if (suspect_testrun.started) {
    suspect_suite_summarize();
  } else {
    suspect_testrun.started = 1;
    __suspect_suite_tally();
  }
  suspect_testrun.current_suite.name = suite_name;
}

void suspect_summarize() {
  if (suspect_testrun.current_suite.tests_total > 0 &&
      suspect_testrun.started) {
    suspect_suite_summarize();
  } else {
    __suspect_suite_tally();
  }
  fprintf(stdout, "\n");
  fprintf(stdout, "total: %d tests, %d assertions\n",
          suspect_testrun.aggregate.tests_total,
          suspect_testrun.aggregate.assertions_total);
  fprintf(stdout, "pass: %d, fail: %d, skip: %d\n",
          suspect_testrun.aggregate.tests_passed,
          suspect_testrun.aggregate.tests_failed,
          suspect_testrun.aggregate.tests_skipped);
}

void __suspect_run_test(void (*test_fn)(void), const char *func,
                        const char *file) {
  if (suspect_testrun.spec_count > 0) {
    bool should_run = false;
    for (int i = 0; i < suspect_testrun.spec_count; i++) {
      if (strcmp(func, suspect_testrun.specs[i]) == 0 ||
          strstr(file, suspect_testrun.specs[i])) {
        should_run = true;
        break;
      }
    }
    if (!should_run) {
      return;
    }
  }
  if (suspect_testrun.current_suite.tests_total == 0 &&
      suspect_testrun.current_suite.name) {
    fprintf(stdout, "\n> %s\n  ", suspect_testrun.current_suite.name);
  }
  suspect_testrun.current = (suspect_test){};
  suspect_testrun.current_suite.tests_total++;
  test_fn();
  if (suspect_testrun.current.assertions_failed > 0) {
    suspect_testrun.current_suite.tests_failed++;
    fprintf(stdout, "F");
  } else if (suspect_testrun.current.assertions_total > 0) {
    suspect_testrun.current_suite.tests_passed++;
    fprintf(stdout, ".");
  } else {
    suspect_testrun.current_suite.tests_skipped++;
    fprintf(stdout, "S");
  }
  suspect_testrun.current_suite.assertions_total +=
      suspect_testrun.current.assertions_total;
}

int __suspect_strlen(const char *str) {
  int length = 0;
  char next;
  while ((next = str[length++]) && next != '\0' && length < STRLEN_MAX) {
  }
  return length;
}

void __suspect_assert(bool result, const char *func, const char *file,
                      long lineno, const char *format, ...) {
  suspect_testrun.current.assertions_total++;
  if (!result) {
    suspect_testrun.current.assertions_failed++;
    size_t format_len = __suspect_strlen(format) + __suspect_strlen(func) +
                        __suspect_strlen(file) + 10;
    char full_format[format_len];
    snprintf(full_format, format_len, "\n%s: %s %s:%lu\n", func, format, file,
             lineno);
    va_list args;
    va_start(args, format);
    vfprintf(stdout, full_format, args);
    va_end(args);
  }
}

void __suspect_assert_streq(const char *val1, const char *val2,
                            const char *func, const char *file, long lineno) {
  if (val1 == NULL && val2 == NULL) {
    __suspect_assert(val1 == val2, func, file, lineno, "NULL != NULL");
  } else if (val1 == NULL) {
    __suspect_assert(val1 == val2, func, file, lineno, "NULL != '%s'", val2);
  } else if (val2 == NULL) {
    __suspect_assert(val1 == val2, func, file, lineno, "'%s' != NULL", val1);
  } else {
    char char1, char2;
    bool equal = true;
    for (int i = 0, char1 = val1[i], char2 = val2[i];;
         ++i, char1 = val1[i], char2 = val2[i]) {
      if (char1 != char2) {
        equal = false;
        break;
      }
      if (char1 == '\0') {
        break;
      }
    }
    __suspect_assert(equal, func, file, lineno, "'%s' != '%s'", val1, val2);
  }
}

#ifdef SUSPECT_AUTODISCOVERY
#if defined(_WIN32)
#error "Autodiscovery is only supported on POSIX platforms at this time"
#endif
#include <stdint.h> // for uintptr_t
#include <string.h>

#ifndef SUSPECT_NM_PATH
#define SUSPECT_NM_PATH "/usr/bin/nm"
#endif

#ifndef SUSPECT_SUITE_MATCHER
#define SUSPECT_SUITE_MATCHER "test_suite_"
#endif

#if defined(__APPLE__)
#define SUSPECT_SUITE_MATCHER_PREFIX "_" SUSPECT_SUITE_MATCHER
#else
#define SUSPECT_SUITE_MATCHER_PREFIX SUSPECT_SUITE_MATCHER
#endif

FILE *__suspect_symbol_table(char *path) {
  int len = strlen(path) + sizeof(SUSPECT_NM_PATH) + 17;
  char cmd[len];
  memset(cmd, 0, len);
  sprintf(cmd, "%s -p -P %s 2>&1", SUSPECT_NM_PATH, path);
  return popen(cmd, "r");
}

uintptr_t __suspect_load_address() {
  uintptr_t address = 0;
  char *line = NULL, *ctx;
  size_t line_len;
  char cmd[256];
  memset(cmd, 0, 256);
  int pid = getpid();
#if defined(__APPLE__)
  sprintf(cmd, "%s %d", "/usr/bin/vmmap -summary", pid);
  FILE *map = popen(cmd, "r");
  while (getline(&line, &line_len, map) != -1) {
    if (strncmp("Load Address:", line, 13) == 0) {
      strtok_r(line, " ", &ctx); // 'Load'
      strtok_r(NULL, " ", &ctx); // 'Address:'
      char *load_addr = strtok_r(NULL, " ", &ctx);
      sscanf(load_addr, "%lx", &address);
      break;
    }
  }
  pclose(map);
#elif defined(__linux__)
  /*
  sprintf(cmd, "cat /proc/%d/maps", pid);
  FILE *map = popen(cmd, "r");
  getline(&line, &line_len, map);
  while (getline(&line, &line_len, map) != -1) {
    printf("%s", line);
  }
  */
#endif
  return address;
}

typedef void (*suspect_test_fn)(void);

suspect_test_fn __suspect_find_test_suite(FILE *output,
                                          uintptr_t load_address) {
  char *line = NULL;
  size_t line_len;
  while (getline(&line, &line_len, output) != -1) {
    if (strncmp(SUSPECT_SUITE_MATCHER_PREFIX, line,
                sizeof(SUSPECT_SUITE_MATCHER_PREFIX) - 1) == 0) {
      char *ctx;
      char *symbol_name = strtok_r(line, " ", &ctx);
      if (!symbol_name)
        continue;
      char *symbol_type = strtok_r(NULL, " ", &ctx);
      if (!symbol_type || !(symbol_type[0] == 'T' || symbol_type[0] == 't'))
        continue;
      char *symbol_addr = strtok_r(NULL, " ", &ctx);
      if (!symbol_addr)
        continue;
      int offset;
      int matches = sscanf(symbol_addr, "%x", &offset);
      if (matches) {
        return (void (*)(void))(offset + load_address);
      }
    }
  }
  return NULL;
}

int main(int argc, char *argv[]) {
  suspect_setup(argc, argv);

  FILE *symbols = __suspect_symbol_table(argv[0]);
  if (symbols) {
    uintptr_t load_address = __suspect_load_address();
    void (*test_fn)(void);
    while ((test_fn = __suspect_find_test_suite(symbols, load_address))) {
      (*test_fn)();
    }
    pclose(symbols);
  }

  suspect_summarize();

  return suspect_exit_code();
}
#endif // SUSPECT_AUTODISCOVERY

#endif // SUSPECT_IMPLEMENTATION
